class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        -Cant multiply unless necessary
        -maximize reuse of multiplications
        -Memo holding intermediate multiplications?
        -Interesting - memo holding prefix and suffix multiplications
        12345
        """
        product = 1
        outputs = [None] * len(nums)
        outputs[len(nums)-1] = 1
        #suffix array
        for i in range(len(nums)-2,-1,-1):
            product *=nums[i+1]
            outputs[i] = product
        #prefix multiply
        product = 1
        for i in range(1,len(nums)):
            product *= nums[i-1]
            outputs[i] *= product
        return outputs